#!/bin/bash

echo "Script: Installing ansible and python3"
sudo apt-get update -y
sudo apt-get install software-properties-common -y
sudo add-apt-repository --yes --update ppa:ansible/ansible
sudo apt-get install ansible python3 python3-pip -y

sudo ansible-galaxy collection install community.docker -p /usr/lib/python3/dist-packages/ansible_collections --force # Update to the latest community.docker collection to fix docker_compose related issues on community.docker v3.4.11. update to community.docker v3.5.0

echo "Installing boto3 botocore"
pip install -q boto3 botocore # -q is quite / silent install

echo "checking if ~/.aws exist."
if [[ ! -d ~/.aws ]]; then
  echo "creating ~/.aws folder"
  mkdir ~/.aws
fi

if [[ ! -f ~/.aws/credentials ]]; then
  echo "creating ~/.aws/credentials"
  touch ~/.aws/credentials
  echo "[default]" > ~/.aws/credentials
fi

echo "populating the ~/.aws/credentials"
echo "aws_access_key_id = ${AWS_ACCESS_KEY_ID}" >> ~/.aws/credentials
echo "aws_secret_access_key = ${AWS_SECRET_ACCESS_KEY}" >> ~/.aws/credentials