vpc_cidr_block    = "10.0.0.0/16"
ssh_key_name      = "$AWS_KEY_NAME"
subnet_cidr_block = "10.0.0.0/24"
avail_zone        = "ap-southeast-1a"
env_prefix        = "dev"
my_public_ip      = ["$MY_PUBLIC_IP", "$JENKINS_IP"]
instance_type     = "t2.small"
