terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.19.0"
    }
  }
}

provider "aws" {
  region = "ap-southeast-1"
  # access_key = "test"
  # secret_key = "test"

  endpoints {
    # apigateway     = "http://localhost:4566"
    # ec2            = "http://localhost:4566"
    # cloudformation = "http://localhost:4566"
    # cloudwatch     = "http://localhost:4566"
    # dynamodb       = "http://localhost:4566"
    # es             = "http://localhost:4566"
    # firehose       = "http://localhost:4566"
    # iam            = "http://localhost:4566"
    # kinesis        = "http://localhost:4566"
    # lambda         = "http://localhost:4566"
    # route53        = "http://localhost:4566"
    # redshift       = "http://localhost:4566"
    # s3             = "http://localhost:4566"
    # secretsmanager = "http://localhost:4566"
    # ses            = "http://localhost:4566"
    # sns            = "http://localhost:4566"
    # sqs            = "http://localhost:4566"
    # ssm            = "http://localhost:4566"
    # stepfunctions  = "http://localhost:4566"
    # sts            = "http://localhost:4566"
  }
}

terraform {
  required_version = ">= 1.6.1"
  backend "s3" {
    bucket = "twwn-capstore-proj"
    key    = "state-capstone-proj05.tfstate"
    region = "ap-southeast-1"
  }
}
