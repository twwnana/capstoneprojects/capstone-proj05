variable "vpc_cidr_block" {}
variable "subnet_cidr_block" {}
variable "avail_zone" {}
variable "env_prefix" {}
variable "my_public_ip" {}
variable "instance_type" {}
variable "ssh_key_name" {}

resource "aws_vpc" "myapp-vpc" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_hostnames = true

  tags = {
    Name : "${var.env_prefix}-vpc"
  }
}

resource "aws_subnet" "myapp-subnet-1" {
  vpc_id            = aws_vpc.myapp-vpc.id
  cidr_block        = var.subnet_cidr_block
  availability_zone = var.avail_zone

  tags = {
    Name : "${var.env_prefix}-subnet-1"
  }
}

resource "aws_route_table_association" "a-rtb-subnet" {
  subnet_id      = aws_subnet.myapp-subnet-1.id
  route_table_id = aws_route_table.myapp-route-table.id
}

resource "aws_internet_gateway" "myapp-igw" {
  vpc_id = aws_vpc.myapp-vpc.id
}

resource "aws_route_table" "myapp-route-table" {
  vpc_id = aws_vpc.myapp-vpc.id

  route {
    # the cidr block associated with the vpc will automatically be created.

    # default route (internet route)
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myapp-igw.id
  }

  tags = {
    Name : "${var.env_prefix}-igw"
  }
}

resource "aws_security_group" "myapp-sg" {
  name   = "myapp-sg"
  vpc_id = aws_vpc.myapp-vpc.id

  # rule for SSH
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = var.my_public_ip
  }

  # rule for application
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name : "${var.env_prefix}-igw"
  }
}

data "aws_ami" "latest_amzn_linux-image2" {
  most_recent = true
  owners      = ["amazon"]

  # further filter the qualified images.
  filter {
    name   = "image-id" # the property to use to filter, check "aws cli" to check what propety you can use to refer by querying the ami objects (aws ec2 describe-images).
    values = ["ami-0fa377108253bf620"]
  }

  # filter can be speficied multiple times for a different criteria
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

# EC2 Instance
resource "aws_instance" "ansible-control-node" {
  ami           = data.aws_ami.latest_amzn_linux-image2.id
  instance_type = var.instance_type

  subnet_id              = aws_subnet.myapp-subnet-1.id
  vpc_security_group_ids = [aws_security_group.myapp-sg.id]
  availability_zone      = var.avail_zone

  associate_public_ip_address = true

  #user_data = file("entry-script.sh")

  key_name = var.ssh_key_name

  tags = {
    Name : "ansible-control-node"
    Env : "control-node"
  }
}

resource "aws_instance" "ansible-managed-node01" {
  ami           = data.aws_ami.latest_amzn_linux-image2.id
  instance_type =var.instance_type

  subnet_id              = aws_subnet.myapp-subnet-1.id
  vpc_security_group_ids = [aws_security_group.myapp-sg.id]
  availability_zone      = var.avail_zone

  associate_public_ip_address = true

  #user_data = file("entry-script.sh")

  key_name = var.ssh_key_name

  tags = {
    Name : "ansible-managed-node01"
    Env : "managed-node"
  }
}

resource "aws_instance" "ansible-managed-node02" {
  ami           = data.aws_ami.latest_amzn_linux-image2.id
  instance_type = var.instance_type

  subnet_id              = aws_subnet.myapp-subnet-1.id
  vpc_security_group_ids = [aws_security_group.myapp-sg.id]
  availability_zone      = var.avail_zone

  associate_public_ip_address = true

  #user_data = file("entry-script.sh")

  key_name = var.ssh_key_name

  tags = {
    Name : "ansible-managed-node02"
    Env : "managed-node"
  }
}

output "ec2_public_ip-ansible-control-node" {
  value = aws_instance.ansible-control-node.public_ip
}

output "ec2_public_ip-ansible-managed-node01" {
  value = aws_instance.ansible-managed-node01.public_ip
}

output "ec2_public_ip-ansible-managed-node02" {
  value = aws_instance.ansible-managed-node02.public_ip
}

output "instance-id-ansible-control-node" {
  value = aws_instance.ansible-control-node.id
}

output "instance-id-ansible-managed-node01" {
  value = aws_instance.ansible-managed-node01.id
}

output "instance-id-ansible-managed-node02" {
  value = aws_instance.ansible-managed-node02.id
}