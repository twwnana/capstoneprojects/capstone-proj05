#!/bin/bash

# Wait for the EC2 instance to be ready to accept connections
echo "Creating the EC2 instance."
while ! aws ec2 wait instance-status-ok --instance-ids $1 $2 $3 --region ap-southeast-1 >/dev/null 2>&1; do 
  sleep 5; 
  echo "Waiting for EC2 instance to be ready"
done