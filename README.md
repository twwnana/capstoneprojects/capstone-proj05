# Introduction

This project is a sample implementation of all the knowledge learned in the DevOps Bootcamp from Techworld with Nana for Module 15.

The objectives here are similar to what is being asked on the Capstore Project 5.

<img width="650" alt="petclinic-screenshot" src="https://lh6.googleusercontent.com/WnLz5XO8sk9o54djeP5Z9CjfFsZfCIR5caBjfw8ZwIWR6iA12QfWF5Rtq9BzeVcEBnEU8QiIHlAH6XKRSDN9BTxkzdQqZy5DHGLj5cK6jnswWB6ONY1XteCKNNyHDXEkEQ=w529">

NOTE: We are using terraform to build EC2 instances for simplicity and easier cleanup and using ansible to configure the instances.

The project is very similar to the contents below, the main differences are:
1. The AWS images are using Ubuntu.
2. There is 1 Ansible Server Node and 2 Ansible Managed Nodes.

Bootcamp Chapter 11 Git Repo: git@gitlab.com:twwnana/aws-eks/chapter11.git

NOTE: At the time of making this project, Python Docker SDK 7.0 has issues working with ansible.builtin.pip module, you will notice that the python docker install is fixated to 6.1.3 as a workaround.

Reference: https://github.com/geerlingguy/internet-pi/issues/567

# Requirement before running the terraform module
- Create the Key pair object in AWS. 

# Pipeline parameters

There are several pipeline that you need to provide the pipeline for a successful deployment.
- MY_PUBLIC_IP - This will be the public IP where SSH client connections will be allowed from (used together with SSH Key). This must be in the CIDR format (like 999.999.999.999/32).
- JENKINS_IP - Is the IP address of the Jenkins server. This must be in the CIDR format (like 999.999.999.999/32).
- SSH_KEY_NAME - Is the SSH Key to be assigned to the Ansible Control node and the 2 EC2 managed nodes.
- ECR_PASS - The ECR Password to use to retrieve the container image 951865328368.dkr.ecr.ap-southeast-1.amazonaws.com/docker-exercise:java-maven-app-1.2.

# Testing the application Output

NOTE: The app is deployed on 2 different EC2 instance and does not share database.

Once the pipeline is completely deployed. It may take a minute or two before the app is running on the EC2 instance.
You can try to test the app by using the addresses below.

http://ec2_node_ip1:8080

http://ec2_node_ip2:8080

# Tech Stack

Infrastructure: AWS ECR, AWS EC2

Technologies: Containerization (Docker, Docker Compose and AWS ECR), CICD Pipeline (Jenkins), Git (Code Versioning), Maven (application), Terraform (AWS EC2 provisioning)