pipeline {
  agent any
  
  parameters {
    text(name: 'MY_PUBLIC_IP', defaultValue: '0.0.0.0/0', description: 'The public IP where SSH is allowed.')
    text(name: 'JENKINS_IP', defaultValue: '170.64.170.203/32', description: 'The public IP where SSH is allowed.')
    text(name: 'SSH_KEY_NAME', defaultValue: 'capstone-proj05-ansible', description: 'The SSH key pair to use in AWS.')
    text(name: 'ECR_PASS', defaultValue: '', description: 'The ECR Password to use.')
  }
  
  stages {
    stage("Initiaze Vars") {
      steps {
        script {
          env.AWS_KEY_NAME = "${params.SSH_KEY_NAME}"
          env.MY_PUBLIC_IP = "${params.MY_PUBLIC_IP}"
          env.JENKINS_IP = "${params.JENKINS_IP}"
        }
      }
    }
    stage('Deploy EC2 Enpoints') {
      steps {
        script {
          dir("terraform") {
            sh "envsubst < terraform-template.tfvars > terraform.tfvars"
            
            sh "terraform init -migrate-state"
            sh "terraform apply -var-file terraform.tfvars -auto-approve"

            env.ANSIBLE_SERVER_IP = sh(
              script: 'terraform output ec2_public_ip-ansible-control-node',
              returnStdout: true
            ).trim()
            
            env.ANSIBLE_SERVER_ID = sh(
              script: 'terraform output instance-id-ansible-control-node',
              returnStdout: true
            ).trim()
            
            env.MANAGED_NODE01_ID = sh(
              script: 'terraform output instance-id-ansible-managed-node01',
              returnStdout: true
            ).trim()
            
            env.MANAGED_NODE02_ID = sh(
              script: 'terraform output instance-id-ansible-managed-node02',
              returnStdout: true
            ).trim()

            sh "./wait-for-ec2-script.sh ${env.ANSIBLE_SERVER_ID} ${env.MANAGED_NODE01_ID} ${env.MANAGED_NODE02_ID}"
          }
        }
      }
    }

    stage('Copy files to ansible server') {
      environment {
          AWS_ACCESS_KEY_ID=credentials('AWS_ACCESS_KEY_ID_ST')
          AWS_SECRET_ACCESS_KEY=credentials('AWS_SECRET_ACCESS_KEY_ST')
      }
      steps {
        script {
          echo "copying all necessary fiels to ansible control node."
          
          sshagent(['ansible-ssh-key']) { // SSH to the Ansible Server
            sh "envsubst < ansible/prepare-ansible-server.sh > ansible/prepare-ansible-server-populated.sh"
            sh "chmod u+x ansible/prepare-ansible-server-populated.sh"

            sh "scp -o StrictHostKeyChecking=no ansible/* ubuntu@${env.ANSIBLE_SERVER_IP}:/home/ubuntu" 

            // Copy the private keys to SSH to the AWS endpoints
            withCredentials([sshUserPrivateKey(credentialsId: 'ansible-ssh-key', keyFileVariable: 'keyfile', usernameVariable: 'user')]) {
              sh 'scp -o StrictHostKeyChecking=no ${keyfile} ubuntu@' + "${env.ANSIBLE_SERVER_IP}" + ':/home/ubuntu/capstone-proj05-ansible.pem'
            }
          }          
        }
      }
    }

    stage("execute ansible playbook") { 
      steps {  
        script {
          echo "calling ansbile playbook to configure ec2 instances"
      
          def ansible_node = [:]
          ansible_node.name = "ansible-control-node"
          ansible_node.host = env.ANSIBLE_SERVER_IP.toString().replace("\"", "") // must be entereted this way, otherwise the SSH-step plugin will treat the variable as string isntead of IP.
          
          ansible_node.allowAnyHosts = true // Equivalent to StrictHostKeyChecking=no    

          withCredentials([sshUserPrivateKey(credentialsId: 'ansible-ssh-key', keyFileVariable: 'keyfile', usernameVariable: 'user')]) {
            ansible_node.user = user
            ansible_node.identityFile = keyfile
            
            echo "Running commands / scripts remotely on Ansible Server."
            // sshCommand remote: ansible_node, command: 'ls -la', sudo: false
            
            sshScript remote: ansible_node, script: "ansible/prepare-ansible-server-populated.sh"
            sshCommand remote: ansible_node, command: 'ansible-inventory -i inventory_aws_ec2.yaml --graph'
            sshCommand remote: ansible_node, command: "ansible-playbook deploy-docker-generic.yaml -e \"ecr_username=AWS ecr_password=${params.ECR_PASS} DB_ROOT_PASSWORD=Welcome123! DB_USER_USERNAME=admin DB_USER_PASSWORD=Welcome123! DB_NAME=mydb DB_ENDPOINT=mysql\""
            sshCommand remote: ansible_node, command: 'rm prepare-ansible-server-populated.sh'
          }
        } 
      }
    }
  }
}